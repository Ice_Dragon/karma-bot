#!/bin/python

import re
import ssl
import sys
import json
import socket
import logging
import threading
from time import sleep

server = "lavender.darkscience.net"
port = 6697
botnick = "kBot"
channel = "#darkscience"

def connect():
    global irc
    irc = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM))
    while True:
        try:
            print("[SERVER] Attempting to connect....")
            irc.connect((server, port))
            irc.send("USER "+ botnick +" "+ botnick +" "+ botnick +" : Karma Bot | By Ice_Dragon \n")
            irc.send("NICK "+ botnick +"\n")
        except socket.error:
            print(socket.error)
            sleep(30)
            continue
        break

def joinChan(channel):
    print("[SERVER] Attemping to join: " + channel)
    irc.send("JOIN "+ channel + "\n")

connect()



###############################
#   Create Karma process      #
###############################

karma_val = []
karma_num = []


def karmaload():
    global karma_val
    global karma_num
    load_val = open("karma_val.json")
    load_num = open("karma_num.json")
    karma_val = json.loads(load_val.read())
    karma_num = json.loads(load_num.read())
    load_val.close()
    load_num.close()
    print("[SERVER] Loaded Karma File")


def karmasave():
    threading.Timer(30, karmasave).start()
    global karma_val
    global karma_num
    save_val = file("karma_val.json", "w")
    save_num = file("karma_num.json", "w")
    save_val.write(json.dumps(karma_val))
    save_num.write(json.dumps(karma_num))
    save_val.close()
    save_num.close()
    print("[SERVER] Saved Karma File")
try:
    karmasave()
    karmaload()
except BaseException():
    logging.critical('Karmaload failed to start')
    karmasave()
    
else:
    karmasave()


####################################################
#          Build karma functions for IRC           #
####################################################


def karmaup():
    try:
        karma_up = (text.split("++")[0]).split(":")[2].rsplit(None, 1)[-1].lower()
    except IndexError:
        message = "What would you like to give Karma to? (e.g. kBot++)"
        irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')
        return
    if karma_up in karma_val:
        idx = karma_val.index(karma_up)
        num = karma_num[idx]
        karma_num[idx] = int(num) + 1
        user = (text.split(":")[1]).split("!")[0]
        irc.send('PRIVMSG ' + channel + ' :' + user + " gave karma to " + karma_up + " (++)" + '\r\n')
    elif karma_up not in karma_val:
        karma_val.append(karma_up)
        karma_num.append(1)
        user = (text.split(":")[1]).split("!")[0]
        irc.send('PRIVMSG ' + channel + ' :' + user + " gave karma to " + karma_up + " (++)" + '\r\n')

def karmadown():
    try:
        karma_down = (text.split("--")[0]).split(":")[2].rsplit(None, 1)[-1].lower()
    except IndexError:
        message = "What would you like to take Karma away from? (e.g. kBot--)"
        irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')
        return
    if karma_down in karma_val:
        idx = karma_val.index(karma_down)
        num = karma_num[idx]
        karma_num[idx] = int(num) - 1
        user = (text.split(":")[1]).split("!")[0]
        irc.send('PRIVMSG ' + channel + ' :' + user + " took karma from " + karma_up + " (--)" + '\r\n')
    elif karma_down not in karma_val:
        karma_val.append(karma_down)
        karma_num.append(-1)
        user = (text.split(":")[1]).split("!")[0]
        irc.send('PRIVMSG ' + channel + ' :' + user + " took karma from " + karma_up + " (--)" + '\r\n')


def karmarank():
    try:
        rank = (text.split(':!rank')[1]).strip().lower()
    except IndexError:
        message = "What would you like to check the rank of? (e.g. !rank kBot)"
        irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')
        return
    if rank in karma_val:
        idx = karma_val.index(rank)
        num = karma_num[idx]
        message = (rank + " has " + str(num) + " points of karma!")
        irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')
    if rank == "":
        print("[SERVER] Found value of rank:" + rank)
        user = text.split("!", 1)[0].replace(":", "", 1)
        rank = user.lower()
        if rank in karma_val:
            print"[SERVER] Found user in blank: " + user
            idx = karma_val.index(rank)
            num = karma_num[idx]
            message = (rank + " has " + str(num) + " points of karma!")
            irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')
        elif rank not in karma_val:
            message = (rank + " doesn't have any karma yet!")
            irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')

    elif rank not in karma_val:
        message = (rank + " doesn't have any karma yet!")
        irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')
    


def topkarma():
    top_results = sorted(zip(karma_num, karma_val), reverse=True)[:3]
    for (x, y) in top_results:
        message = (y + ": " + str(x))
        irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')


def bottomkarma():
    top_results = sorted(zip(karma_num, karma_val), reverse=False)[:3]
    for (x, y) in top_results:
        message = (y + ": " + str(x))
        irc.send('PRIVMSG ' + channel + ' :' + message + '\r\n')


###############################
#   End Karma Process         #
###############################

while True:
    text = irc.recv(1024)
    print(text)
    text = text.strip('\n\r')
 
    if text.find('PING') != -1:
        irc.send('PONG ' + text.split()[1] + '\r\n')
    if text.find('darkscience.net 376 kBot :End of message of the day.') != -1:
        joinChan(channel)
    if text.find(':vermillion.darkscience.net 366 kBot') != -1:
        irc.send("MODE kBot +B")
    if text.find('++') != -1 and text.find(channel) != -1:
        user = text.split("!", 1)[0].replace(":", "", 1)
        karma_up = (text.split("++")[0]).split(":")[2].rsplit(None, 1)[-1]
        if user == karma_up:
            irc.send('PRIVMSG ' + channel + ' :' + "You can't give Karma to yourself!" + '\r\n')    
        else:
            karmaup()
    if text.find('--') != -1 and text.find(channel) != -1:
        user = text.split("!", 1)[0].replace(":", "", 1)
        karma_up = (text.split("++")[0]).split(":")[2].rsplit(None, 1)[-1]
        if user == karma_up:
            irc.send('PRIVMSG ' + channel + ' :' + "You can't give Karma to yourself!" + '\r\n')
        else:
            karmadown()
    if text.find(':!rank') != -1 and text.find(channel) != -1:
        karmarank()
    if text.find(':!top') != -1 and text.find(channel) != -1:
        topkarma()
    if text.find(':!bottom') != -1 and text.find(channel) != -1:
        bottomkarma()
    if text.find(':!quit') != -1:
       user = text.split("!", 1)[0].replace(":", "", 1)
       print(user)
       if user == "Ice_Dragon" != -1:
           karmasave()
           irc.close()
           sys.exit()
              

